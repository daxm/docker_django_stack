Django==3.1.5
gunicorn==20.0.4
django-extensions==3.1.0
ipython==7.19.0
django-grappelli==2.14.3
django-debug-toolbar==3.2
sentry-sdk==0.19.5
dj-database-url==0.5.0
