# Docker Django Stack

## Provides
* Django
  - [Gunicorn](https://github.com/benoitc/gunicorn) WSGI (HTTP)
  - [Daphne](https://github.com/django/daphne) ASGI (WebSockets)

* [Huey](https://huey.readthedocs.io/en/latest/) task queue
* PostgreSQL Database
* [Redis](https://redis.io/) key=value store
* Nginx front-end HTTPS server

## How to use:
1. Fork this repository.
2. Create the django project using manage.py with a [template](https://gitlab.com/rpmrpm/django_template)
>> ```cd django && django-admin.py startproject --template https://gitlab.com/rpmrpm/django_template/-/archive/master/django_template-master.zip PROJECT_NAME .```

**DON'T FORGET THE TRAILING .**

## Directory Structure
* docker-compose.yml
  - Docker Compose file to build project
  >  ```docker-compose build``` or ```docker-compose up -d``` to build, run and detach


* django/
  - **Django project**, requirements.txt, [custom apps](django/apps) and [Dockerfile](django/Dockerfile)


* django/run
  - Files needed to 'run' the project. **MEDIA_ROOT** and **STATIC_ROOT** directories.
    This volume is shared across all django containers in this Compose file
    

* nginx/
  - Nginx config files and [Dockerfile](nginx/Dockerfile)

  - URL Routing:
     1. /static -> /django/run/static
     2. /media -> /django/run/media
     3. /ws -> Daphne ASGI Server
     4. Try to find a matching file in /nginx/html
     5. Send to Gunicorn WSGI Server - see [django/urls.py](django/urls.py)
